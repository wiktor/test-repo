---
title: "OpenPGP fingerprints in the DNS"
abbrev: "PGP fingerprints in the DNS"
ipr: "trust200902"
submissiontype: "independent"
keyword: [""]
date: 2024-02-16T10:27:00Z

seriesInfo:
  name: "Internet-Draft"
  value: "draft-00"
  stream: "independent"
  status: "informational"

authors:
  - initials: "W."
    surname: "Kwapisiewicz"
    fullname: "Wiktor Kwapisiewicz"
    organization: "Metacode"
    role: "editor"
    address:
     email: "wiktor@metacode.biz"
---



# Test Repo

```mermaid
    stateDiagram
    [*] --> Active

    state Active {
        [*] --> NumLockOff
        NumLockOff --> NumLockOn : EvNumLockPressed
        NumLockOn --> NumLockOff : EvNumLockPressed
    }
```

Is this OK?

Raw:

```rust
fn main() {}
```

With spaces:

```rust file=test.rs id=3 comment=test
fn main() {}
```

With commas:

```rust,file=test.rs,id=3,comment=test
fn main() {}
```

test
test test
test test test
